import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { catchError, retry } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { AppSettings } from '../../AppSetiings';


@Injectable()
export class AuthenticationService {

  options: RequestOptions;
  isOTPInvalid: boolean = true;

  constructor(private http: Http, private router: Router) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('dataType', 'text');
    this.options = new RequestOptions({ headers: headers, withCredentials: true });
  }

  performLogin(username, password) {    
    const reqBody = `j_username=${username}&j_password=${password}`;
    return this.http.post(`${AppSettings.baseUrl}authenticate`, reqBody, this.options)
      .subscribe(response => {      
        if (response.status === 200) {
          this.router.navigate(["/dashboard"]);
          console.log("User logged in successfully");
        } else {
          alert("Received non 200 status");
        }
      },
        error => {
          switch (error.status) {
            case 302: {
              this.router.navigate(["/otp"]);
              console.log("Redirecting to enter otp screen");
              break;
            }
            case 401: {
              alert("Opps! Wrong username or password entered.");
              break;
            }
            default: {
              console.log("Failed to login")
            }
          }
        })
  }

  performLogout() {
    console.log("Performing logout operation")
    this.http.post(`${AppSettings.baseUrl}logout`, this.options)
      .subscribe(response => {
        console.log("User looged out")
        this.router.navigate(["/login"]);
      })
  }

  sendOTPToServer(otp: number) {
    let reqBody = `totp=${otp}`;
    this.http.post(`${AppSettings.baseUrl}authenticate/validateOTP`, reqBody, this.options)
      .subscribe(response => {
        if (response.status === 200) {
          this.router.navigate(["/dashboard"]);
          console.log("User logged in successfully");
        } else {
          alert("Received non 200 status");
        }
      }, error => {
        switch (error.status) {
          case 400: {
            console.error("Wrong otp entered: Please enter correct otp")
            alert("Opps! Invalid otp entered");
            break;
          }
          case 401: {
            this.router.navigate(["/login"]);
            break;
          }
          default: {
            console.log("Failed to login")
          }
        }
      })
  }
}
