import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers} from '@angular/http';
import { AppSettings } from '../../AppSetiings';
import { Currency } from '../../components/Currency';

@Injectable()
export class CurrencyService {
  options;

  private url = `${AppSettings.baseUrl}currencies`;
  constructor(private http: Http) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.options = new RequestOptions({ headers: headers, withCredentials: true});
  }

  getAll() {
    return this.http.get(`${this.url}/all`, this.options)
  }

  saveCurrency(currency){
    return this.http.post(this.url, currency, this.options);
  }

  updateCurrency(currency){
    return this.saveCurrency(currency);
  }

  getCurrency(currencyId){
    return this.http.get(`${AppSettings.baseUrl}currencies/${currencyId}`, this.options);
  }
}
