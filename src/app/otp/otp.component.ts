import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication/authentication.service';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent implements OnInit {

  otp:number;
  isOTPValide:boolean;

  constructor(private authService: AuthenticationService) { 
    this.isOTPValide = authService.isOTPInvalid;
  }

  onOTPEnter(){
    this.authService.sendOTPToServer(this.otp);
  }

  ngOnInit() {
  }

}
