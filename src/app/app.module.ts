import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule } from './app-routing.module';
import { LoginService } from './login.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpModule } from '@angular/http';
import { OtpComponent } from './otp/otp.component';
import { AuthenticationService } from './services/authentication/authentication.service';
import { CurrencyService } from './services/currency/currency.service';


@NgModule({
  declarations: [
    AppComponent,  
    LoginComponent,
    DashboardComponent,    
    OtpComponent
  ],
  imports: [
  
  BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    NgbModule.forRoot()
  ],
  providers: [
    LoginService,
    AuthenticationService,
    CurrencyService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
