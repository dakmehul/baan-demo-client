export class Currency {

    private decSept: any[] = [
        {
            value: 1,
            name: "Period"
        },
        {
            value: 2,
            name: "Comma"
        },
        {
            value: 3,
            name: "Space"
        }];
    private thosndSept: any[] = this.decSept;

    constructor(private _curncyId?: string,
        private _crncyDsc?: string,
        private _crncyDscA?: string,
        private _crncySym?: string,
        private _inclSpac: boolean = false,
        private _negSymbl: number = 1,
        private _ngsmampc: number = 1,
        private _desSymbl: number = 1,
        private _thousSymbl: number = 1,
        private _curText1?: string,
        private _curTextA1?: string,
        private _curText2?: string,
        private _curTextA2?: string,
        private _curText3?: string,
        private _curTextA3?: string) { }

    get curncyId() {
        return this.curncyId;
    }
    set curncyId(curncyId) {
        this.curncyId = curncyId;
    }

    get crncyDsc() {
        return this.crncyDsc;
    }
    set crncyDsc(crncyDsc) {
        this.crncyDsc = crncyDsc;
    }

    get crncyDscA() {
        return this.crncyDscA;
    }
    set crncyDscA(crncyDscA) {
        this.crncyDscA = crncyDscA;
    }

    get crncySym() {
        return this.crncySym;
    }
    set crncySym(crncySym) {
        this.crncySym = crncySym;
    }

    // get inclSpac() {
    //     return this.inclSpac;
    // }
    // setInclSpac() {
    //     this.inclSpac = !this.inclSpac;
    // }

    get curText1() {
        return this._curText1
    }
    set curText1(curText1) {
        this.curText1 = curText1;
    }

    get curText2() {
        return this.curText2;
    }
    set curText2(curText2) {
        this.curText2 = curText2;
    }

    get curText3() {
        return this.curText3
    }
    set curText3(curText3) {
        this.curText3 = curText3
    }

    get curTextA1() {
        return this.curTextA1;
    }
    set curTextA1(curTextA1) {
        this.curTextA1 = curTextA1;
    }

    get curTextA2() {
        return this.curTextA2;
    }
    set curTextA2(curTextA2) {
        this.curTextA2 = curTextA2;
    }

    get curTextA3() {
        return this.curTextA3;
    }
    set curTextA3(curTextA3) {
        this.curTextA3 = curTextA3;
    }


}
