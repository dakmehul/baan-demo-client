import { Component, OnInit } from '@angular/core';
import { CurrencyService } from '../services/currency/currency.service';
import { Currency } from '../components/Currency';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { JSONP_ERR_WRONG_RESPONSE_TYPE } from '@angular/common/http/src/jsonp';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  // currency: Currency = new Currency();
  private separators: any[] = [
    {
      value: 1,
      name: "Period"
    },
    {
      value: 2,
      name: "Comma"
    },
    {
      value: 3,
      name: "Space"
    }];
  private negativeSymbols: any[] = [
    {
      value: 1,
      symbol: "()"
    },
    {
      value: 2,
      symbol: "-"
    },
    {
      value: 3,
      symbol: "CR"
    }
  ];
  private selectedCurr = undefined;
  private disSymbols = [

  ];

  decimalSeparators = this.separators.map(d => d.name);
  negSymbols = this.negativeSymbols.map(s => s.symbol);

  curncyId: string;
  crncyDsc: string;
  crncyDscA: string;
  crncySym: string;
  inclSpac: boolean = false;
  negSymbl: String = "()";
  ngsmampc = true;
  desSymbl = 0;
  dispSign = true;
  decimalSeprator = "Period";
  thousandsSeprator = "Period";
  curText1: string;
  curTextA1: string;
  curText2: string;
  curTextA2: string;
  curText3: string;
  curTextA3: string;

  availableCurencies: Currency[] = [];
  constructor(private currencyService: CurrencyService, private authService: AuthenticationService) {
  }

  ngOnInit() {
    this.fetchCurrencies();
  }

  createCureency() {
    this.currencyService.saveCurrency(this.getCurrObj())
      .subscribe(response => {
        if (response.status === 201) {
          this.fetchCurrencies();
          this.onClear();
        }
      },
        error => {
          if (error.status === 409) {
            alert("ID is already used, please enter another id")
          }
          alert("Server failed to create curreny");
          console.log(error)
        })
  }

  getCurrency(currencyId) {
    this.currencyService.getCurrency(currencyId);
  }

  updateCurrency() {
    this.currencyService.saveCurrency(this.getCurrObj());
  }

  onCurrencyId() {
    alert(this.curncyId);
  }

  private getCurrObj() {
    return {
      currnIdx: this.selectedCurr,
      curncyId: this.curncyId,
      crncyDsc: this.crncyDsc,
      crncyDscA: this.crncyDscA,
      crncySym: this.crncySym,
      inclSpac: this.inclSpac,
      negSymbl: this.negativeSymbols.find(nS => {
        return nS.symbol === this.negSymbl;
      }).value,
      ngsmampc: this.ngsmampc ? 1 : 2,
      decSymbl: this.separators.find((s) => {
        return s.name === this.decimalSeprator;
      }).value,
      thousSymbl: this.separators.find((s) => {
        return s.name === this.thousandsSeprator;
      }).value,
      curText1: this.curText1,
      curText2: this.curText2,
      curText3: this.curText3,
      curTextA1: this.curTextA1,
      curTextA2: this.curTextA2,
      curTextA3: this.curTextA3,
      dispSign: this.dispSign ? 1 : 2
    }
  }

  onClear() {
    this.curncyId = undefined;
    this.crncyDsc = undefined;
    this.crncyDscA = undefined;
    this.crncySym = undefined;
    this.inclSpac = false,
      this.negSymbl = "()";
    this.ngsmampc = true;
    this.decimalSeprator = "Period";
    this.thousandsSeprator = "Period";
    this.curText1 = undefined;
    this.curText2 = undefined;
    this.curText3 = undefined;
    this.curTextA1 = undefined;
    this.curTextA2 = undefined;
    this.curTextA3 = undefined;
    this.dispSign = true;
  }

  onDeciamalSeptr(event) {
    this.decimalSeprator = event;
  }

  onThousandsSeptr(event) {
    this.thousandsSeprator = event;
  }

  onNegSymbol(event) {
    this.negSymbl = event;
  }

  fetchCurrencies() {
    this.currencyService.getAll()
      .subscribe(response => {
        this.availableCurencies = response.json();
      },
        error => console.error(error)
      );
  }
  onClickEve() {
    alert("Clicked")
  }

  logout() {
    this.authService.performLogout();
  }

  onCurrClick(currency) {
    this.currencyService.getCurrency(currency.currnIdx)
      .subscribe(result => {
        let response = result.json();
        this.selectedCurr = response["currnIdx"];
        this.curncyId = response["curncyId"];
        this.crncyDsc = response["crncyDsc"];
        this.crncyDscA = response["crncyDscA"];
        this.crncySym = response["crncySym"];
        this.inclSpac = response["inclSpac"];
        this.negSymbl = this.negativeSymbols.find(ns => {
          return ns.value === response["negSymbl"];          
        }).symbol;
        this.ngsmampc = (response["ngsmampc"] === 1 ? true : false);
        this.decimalSeprator = this.separators.find(s => {
          return s.value === response["decSymbl"]
        }).name;
        this.thousandsSeprator = this.separators.find(s => {
          return s.value === response["thousSymbl"]
        }).name;
        this.curText1 = response["curText1"];
        this.curText2 = response["curText2"];
        this.curText3 = response["curText3"];
        this.curTextA1 = response["curTextA1"];
        this.curTextA2 = response["curTextA2"];
        this.curTextA3 = response["curTextA3"];
        this.dispSign = (response["dispSign"] === 1 ? true : false);
      },
        error => {
          alert("Failed to fetch currency")
          console.error(error)
        }
      )
  }

}
