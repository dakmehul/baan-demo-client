import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication/authentication.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userName: string;
  password: string;

  constructor(private authService: AuthenticationService) {
  }

  onLogin() {
    this.authService.performLogin(this.userName, this.password);
  }

  ngOnInit() {
  }

  onUsernameEnter() {
  }

  onPasswordEnter() {
    if (typeof this.userName != 'undefined' && typeof this.password != 'undefined' && this.userName && this.password) {
      this.authService.performLogin(this.userName, this.password);
    } else {
      alert("You missed username or password")
    }
  }

}
